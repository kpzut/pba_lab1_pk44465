# Sprawozdanie LAB 4-6

### Get all users
#### SUCCESS
```
curl -u pk44465:123456 http://localhost:8080/users
```
#### UNAUTHORIZED
```
curl -u pk44465:password http://localhost:8080/users
```
### Add user
#### SUCCESS
```
(konieczność pokazania w Postmanie)
```
#### FORBIDDEN
```
curl -u pk44465:123456 -H "Content-Type: application/json" -H "X-HMAC-SIGNATURE: gGtNskrpd5EfKqkaX11n6w+e7Ko9r7jXH7e4JzXnluA=" --data-raw "{"requestHeader":{"sendDate":"2024-01-09T10:00:00.000+00:00","requestId":"d7478f35-4030-4bd2-8416-cdd5fc7e5536"},"user":{"personalId":"92011165987","name":"Andrzej","surname":"Nowak","age":32,"citizenship":"PL","email":"andrzej.nowak@gmail.com"}}" http://localhost:8080/users
```
### Get user by id
#### SUCCESS
```
curl -u pk44465:123456 http://localhost:8080/users/{id istniejącego usera}
```
#### NOT FOUND
```
curl -u pk44465:123456 http://localhost:8080/users/54f882bb-31bd-4f47-ae01-97ab771c583e
```
### Delete user
#### SUCCESS
```
curl -u pk44465:123456 --request DELETE http://localhost:8080/users/{id istniejącego usera}
```
#### NOT FOUND
```
curl -u pk44465:123456 --request DELETE http://localhost:8080/users/54f882bb-31bd-4f47-ae01-97ab771c583e
```
