package org.openapitools;

import org.openapitools.exceptions.HMAC;
import org.springframework.web.servlet.HandlerInterceptor;
import org.openapitools.filters.HMACRequestBodyWrapper;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;

public class HMACInterceptor implements HandlerInterceptor {

    private static final String secret = "123456";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!"POST".equals(request.getMethod()))
            return true;

        if (!(request instanceof HMACRequestBodyWrapper wrapper)) {
            return true;
        }

        String HMACHeader = request.getHeader("X-HMAC-SIGNATURE");

        if (HMACHeader == null) {
            throw new HMAC("X-HMAC-SIGNATURE header not found");
        }

        String body = wrapper.getRequestBody();
        Mac hmacSha256 = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
        hmacSha256.init(secretKeySpec);
        byte[] hmacBytes = hmacSha256.doFinal(body.getBytes());
        String expectedSignature = Base64.getEncoder().encodeToString(hmacBytes);

        if (!HMACHeader.equals(expectedSignature)) {
            throw new HMAC("X-HMAC-SIGNATURE is invalid");
        }

        return true;
    }
}
