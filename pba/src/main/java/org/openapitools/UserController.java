package org.openapitools;

import org.openapitools.api.UsersApi;
import org.openapitools.exceptions.NotFound;
import org.openapitools.exceptions.Unauthorized;
import org.openapitools.model.*;
import org.openapitools.model.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.Base64;
import java.util.Objects;
import java.util.UUID;

@Controller
public class UserController implements UsersApi {

    private final NativeWebRequest request;

    private String user = "pk44465";

    private String password = "123456";

    private final UserService userService;

    private boolean authorization() {
        String authorizationHeader = request.getHeader("Authorization");

        if (authorizationHeader == null)
            return false;

        if (!authorizationHeader.split(" ")[0].equals("Basic"))
            return false;

        byte[] decoded = Base64.getDecoder().decode(authorizationHeader.split(" ")[1]);
        String credentials = new String(decoded, StandardCharsets.UTF_8);
        String inputUser = credentials.split(":")[0];
        String inputPassword = credentials.split(":")[1];

        return Objects.equals(inputUser, user) && Objects.equals(inputPassword, password);
    }

    @Autowired
    public UserController(NativeWebRequest request, UserService userService) {
        this.request = request;
        this.userService = userService;
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers() {
        if(!authorization())
            throw new Unauthorized("User unauthorized");

        UserListResponse usrLstRsp = new UserListResponse(new RequestHeader(UUID.randomUUID(), OffsetDateTime.now()), userService.getAllUsers());
        return new ResponseEntity<>(usrLstRsp, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(@PathVariable("id") UUID id) {
        if(!authorization())
            throw new Unauthorized("User unauthorized");

        UserResponse usrRsp = new UserResponse(new RequestHeader(UUID.randomUUID(), OffsetDateTime.now()), userService.getUserById(id));
        if (usrRsp.getUser() == null)
            throw new NotFound("User not found");

        return new ResponseEntity<>(usrRsp, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserResponse> createUser(@RequestBody CreateRequest body) {
        if(!authorization())
            throw new Unauthorized("User unauthorized");

        UserResponse usrRsp = new UserResponse(new RequestHeader(body.getRequestHeader().getRequestId(), OffsetDateTime.now()), userService.createUser(body.getUser()));
        return new ResponseEntity<>(usrRsp, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteUser(@PathVariable("id") UUID id) {
        if(!authorization())
            throw new Unauthorized("User unauthorized");

        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
