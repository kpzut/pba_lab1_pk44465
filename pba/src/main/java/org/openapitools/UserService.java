package org.openapitools;

import org.openapitools.exceptions.NotFound;
import org.openapitools.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserService {
    private final List<User> users;

    public UserService() {
        this.users = Stream.of(
            new User("Jan", "Kowalski", 34, "90010112345", User.CitizenshipEnum.PL, "jan.kowalski@gmail.com").id(UUID.randomUUID()),
            new User("Anna", "Nowak", 28, "95021234567", User.CitizenshipEnum.PL, "anna.nowak@gmail.com").id(UUID.randomUUID()),
            new User("Piotr", "Wiśniewski", 45, "78031234567", User.CitizenshipEnum.PL, "piotr.wisniewski@gmail.com").id(UUID.randomUUID()),
            new User("Max", "Schmidt", 35, "89010123456", User.CitizenshipEnum.DE, "max.schmidt@gmail.com").id(UUID.randomUUID())
        ).collect(Collectors.toList());
    }

    public List<User> getAllUsers() {
        return users;
    }

    public User getUserById(UUID id) {
        return users.stream().filter(user -> user.getId().equals(id)).findFirst().orElse(null);
    }

    public User createUser(User user) {
        UUID userId = UUID.randomUUID();
        user.setId(userId);
        users.add(user);
        return user;
    }

    public void deleteUser(UUID id) {
        User selectedUser = users.stream().filter(user -> user.getId().equals(id)).findFirst().orElse(null);
        if (selectedUser == null) {
            throw new NotFound("User not found");
        }

        users.removeIf(user -> user.getId().equals(selectedUser.getId()));
    }

}
