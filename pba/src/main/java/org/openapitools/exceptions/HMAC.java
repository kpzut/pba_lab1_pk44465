package org.openapitools.exceptions;

public class HMAC extends RuntimeException {
    public HMAC(String message) {
        super(message);
    }
}
