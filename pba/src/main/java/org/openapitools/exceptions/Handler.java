package org.openapitools.exceptions;

import org.openapitools.model.ResponseHeader;
import org.openapitools.model.Error;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.OffsetDateTime;
import java.util.UUID;

@ControllerAdvice
public class Handler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handleException(Exception ex) {
        HttpStatus status;
        if (ex instanceof Unauthorized)
            status = HttpStatus.UNAUTHORIZED;
        else if (ex instanceof NotFound)
            status = HttpStatus.NOT_FOUND;
        else if (ex instanceof HMAC)
            status = HttpStatus.FORBIDDEN;
        else
            status = HttpStatus.BAD_REQUEST;

        Error error = new Error()
                .responseHeader(new ResponseHeader().requestId(UUID.randomUUID()).sendDate(OffsetDateTime.now()))
                .code(status.toString())
                .message(ex.getMessage());

        return new ResponseEntity<>(error, status);
    }
}

