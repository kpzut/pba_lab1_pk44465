package org.openapitools.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * UserListResponse
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-12-16T13:32:43.194073300+01:00[Europe/Warsaw]")
public class UserListResponse {

  private RequestHeader responseHeader;

  @Valid
  private List<@Valid User> usersList = new ArrayList<>();

  public UserListResponse() {
    super();
  }

  /**
   * Constructor with only required parameters
   */
  public UserListResponse(RequestHeader responseHeader, List<@Valid User> usersList) {
    this.responseHeader = responseHeader;
    this.usersList = usersList;
  }

  public UserListResponse responseHeader(RequestHeader responseHeader) {
    this.responseHeader = responseHeader;
    return this;
  }

  /**
   * Get responseHeader
   * @return responseHeader
  */
  @NotNull @Valid 
  @Schema(name = "responseHeader", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("responseHeader")
  public RequestHeader getResponseHeader() {
    return responseHeader;
  }

  public void setResponseHeader(RequestHeader responseHeader) {
    this.responseHeader = responseHeader;
  }

  public UserListResponse usersList(List<@Valid User> usersList) {
    this.usersList = usersList;
    return this;
  }

  public UserListResponse addUsersListItem(User usersListItem) {
    if (this.usersList == null) {
      this.usersList = new ArrayList<>();
    }
    this.usersList.add(usersListItem);
    return this;
  }

  /**
   * Get usersList
   * @return usersList
  */
  @NotNull @Valid 
  @Schema(name = "usersList", requiredMode = Schema.RequiredMode.REQUIRED)
  @JsonProperty("usersList")
  public List<@Valid User> getUsersList() {
    return usersList;
  }

  public void setUsersList(List<@Valid User> usersList) {
    this.usersList = usersList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserListResponse userListResponse = (UserListResponse) o;
    return Objects.equals(this.responseHeader, userListResponse.responseHeader) &&
        Objects.equals(this.usersList, userListResponse.usersList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(responseHeader, usersList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserListResponse {\n");
    sb.append("    responseHeader: ").append(toIndentedString(responseHeader)).append("\n");
    sb.append("    usersList: ").append(toIndentedString(usersList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

