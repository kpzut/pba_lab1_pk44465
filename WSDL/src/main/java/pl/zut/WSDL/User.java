package pl.zut.WSDL;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

@Entity
public class User {

    @NotEmpty
    private String imie;

    @NotEmpty
    private String nazwisko;

    @NotEmpty
    private Integer wiek;

    @NotEmpty
    private String email;

    @NotEmpty
    @Size(min = 9, max = 9)
    private String obywatelstwo;

    @NotEmpty
    private String pesel;
    @Id
    private Long id;

    public User() {}

    public User(String username, String password) {
        this.imie = username;
    }

    public String getUsername() {
        return imie;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
