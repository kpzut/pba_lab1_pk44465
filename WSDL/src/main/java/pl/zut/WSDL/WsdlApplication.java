package pl.zut.WSDL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class WsdlApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsdlApplication.class, args);
	}

}
